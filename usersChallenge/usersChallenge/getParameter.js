/* Get the parameters from the current page url
 *
 */
    function getParameter(parameter) {
        let pageURL = window.location.search.substring(1);
        let parameters = pageURL.split('&');
        let parametersCount = parameters.length;
        let parameterName;

        /*
         * Iterate the URL parameters
         */
        for (let k = 0; k < parametersCount; k++) {

            /*
             * Extract the value of the parameter that matches the name
             */
            parameterName = parameters[k].split('=');
            if (parameterName[0] === parameter) {

            /*
             * Handle boolean parameters
             */
                if (parameterName[1] === undefined){
                    return true
                }
                return decodeURIComponent(parameterName[1]);
            }
        }
    };