from django.contrib import admin

# Register your models here.
from .models import Topic, UserProfile, User
admin.site.register(UserProfile)
admin.site.register(Topic)