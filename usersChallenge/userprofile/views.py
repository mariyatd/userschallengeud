from django.shortcuts import render

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from userprofile.models import UserProfile
from userprofile.serializers import UserprofileSerializer

@csrf_exempt
def user_list(request):
    """
    List all userprofiles
    """
    if request.method == 'GET':
        users = UserProfile.objects.all()
        serializer = UserprofileSerializer(users, many=True)
        return JsonResponse(serializer.data, safe=False)

@csrf_exempt
def user_profile(request, pk):
    """
    Retrieve or update a user profile.
    """
    try:
        user = UserProfile.objects.get(pk=pk)
    except UserProfile.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = UserprofileSerializer(user)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = UserprofileSerializer(user, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)
    #
    # elif request.method == 'DELETE':
    #     user.delete()
    #     return HttpResponse(status=204)