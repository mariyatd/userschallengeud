from django.urls import path
from userprofile import views

urlpatterns = [
    path('userprofile/', views.user_list),
    path('userprofile/<int:pk>/', views.user_profile),
]