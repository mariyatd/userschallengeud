from rest_framework import serializers
from userprofile.models import UserProfile
from django.contrib.auth.models import User
from topics.models import Topic
from topics.serializers import TopicSerializer


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('first_name', 'last_name')

class UserprofileSerializer(serializers.ModelSerializer):

    interests = serializers.PrimaryKeyRelatedField(many=True, read_only=False, queryset=Topic.objects.all())
    user = UserSerializer(many=False)

    class Meta:
        model = UserProfile
        fields = ('id', 'position', 'description', 'interests', 'user')

    def create(self, validated_data):
        """
        Create and return a new `userprofile` instance, given the validated data.
        """
        return UserProfile.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `userprofile` instance, given the validated data.
        """

        """
        Handle user manually
        """
        u = User.objects.get(id=validated_data.get('id', instance.id))
        user_data = validated_data.pop('user')
        updated_user = UserSerializer.update(UserSerializer(u), u, validated_data=user_data)
        instance.user = updated_user

        """
        Handle interests manually
        """
        updated_interests = validated_data.pop('interests')
        instance.interests.set(updated_interests)

        instance.description = validated_data.get('description', instance.description)
        instance.position = validated_data.get('position', instance.position)

        instance.save()

        return instance

    """
    Override to_representation to make a request to Topic for every "interest" in the request,
    Needed because I'm updating a many-to-many field and the request contains only ids.
    """
    def to_representation(self, value):
        representation = super().to_representation(value)
        interests = representation.pop('interests')
        updated_interests = []
        for entry in interests:
            topic = Topic.objects.get(pk=entry)
            serializer = TopicSerializer(topic)
            updated_interests.append(serializer.data)
        representation['interests'] = updated_interests
        return representation
