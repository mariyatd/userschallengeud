from django.db import models


class Topic(models.Model):
    name = models.CharField(max_length=12)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name
    # description = models.CharField(max_length=255)
    # name = models.CharField(max_length=60)
    # current_position = models.CharField(max_length=64)

    # class Publication(models.Model):
    #     title = models.CharField(max_length=30)
    #
    #     class Meta:
    #         ordering = ('title',)
    #
    #     def __str__(self):
    #         return self.title