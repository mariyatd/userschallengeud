from django.shortcuts import render

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from topics.models import Topic
from topics.serializers import TopicSerializer

@csrf_exempt
def topic_list(request):
    """
    List all topics
    """
    if request.method == 'GET':
        topics = Topic.objects.all()
        serializer = TopicSerializer(topics, many=True)
        return JsonResponse(serializer.data, safe=False)