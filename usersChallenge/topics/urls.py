from django.urls import path
from topics import views

urlpatterns = [
    path('topics/', views.topic_list),
]