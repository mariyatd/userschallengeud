# from rest_framework.views import APIView
# from rest_framework.response import Response
# from rest_framework import status
# from user.serializers import UserSerializer
# from django.contrib.auth.models import User
#
# class UserCreate(APIView):
#     """
#     Creates the user.
#     """
#
#     def create_user(self, request, format='json'):
#         serializer = UserSerializer(data=request.data)
#         if serializer.is_valid():
#             user = serializer.save()
#             if user:
#                 return Response(serializer.data, status=status.HTTP_201_CREATED)
#



from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from user.serializers import UserSerializer
from django.contrib.auth.models import User

@csrf_exempt
def create_user(request):
    """
    Create a new user.
    """
    # if request.method == 'GET':
    #     users = UserProfile.objects.all()
    #     serializer = UserprofileSerializer(users, many=True)
    #     return JsonResponse(serializer.data, safe=False)

    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            # serializer.pk
            # instance = serializer.save()
            # instance
            # instance_id = instance.pk
            return JsonResponse(serializer.data, status=201)
            # return JsonResponse(serializer.instance, status=201)
        return JsonResponse(serializer.errors, status=400)
