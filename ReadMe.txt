Requirements:
python3, unix based os, git and pip are a good starting point :)

Setup:

git clone https://mariyatd@bitbucket.org/mariyatd/userschallengeud.git # Clone project from bitbucket.
#For the latest version of the project, checkout the "latest" branch

. djangoEnv/bin/activate # activates django environment

sudo python3 -m ensurepip # installs pip without using apt-get, yum, etc

sudo python3 -m pip install django # needed because pip is now a site-package of python3

sudo python3 -m pip install django-cors-headers

sudo python3 -m pip install djangorestframework


Run:
cd usersChallenge
python manage.py runserver # runs server on port 8000.
# If 8000 is occupied, a new port can be passed as a parameter, but 8000 is hardcoded in the frontend

Flow: Open  usersChallenge/register.html file in the browser.
Create your account (please make sure you're using a valid email and a password of at least 8 characters).
This will redirect you to profile edit, where you can add details and choose interests.
When the profile is created, you will be redirected to your profile page.


To create topics, users, or user profiles, go to the Django admin page on localhost:8000/admin



Addressing requirements - What doesn't work and what I would do better:

There's no login implementation. Currently there's no authentication based behavior - a user can edit and see any user profile.

There's no limit to the number of favourite topics one can choose.

There's absolutely no validation of fields or security implemented.

There are no tests whatsoever and very little code documentation

Currently using sqlite for development purposes. Should be mysql.

What I would have done had I had more time

I would have used SASS or LESS for writing CSS faster and more productively

There's some inconsistency with naming for instance. I would spend the time to decide on naming conventions, lint rules etc.
for the project and stick to them. Also I would format the code to be consistent with whitespaces/tabs

I'm currently using no javascript frameworks and there are some outdated practices which should be avoided.
I would spend the time to fix them or implement a framework, such as React, since this solution won't really scale.
On a larger scale frameworks save a lot of time and headaches, because they solve common problems and security concerns.


Also I would spend the time to add files such as .idea/workspace.xml and other unnecessary files to gitignore.

A great thing to have would be automated setup and run script.

